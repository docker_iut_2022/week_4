# week_4

Week 4: introduction to multi-stage builds, and Docker Compose: how to setup a multi-service dockerized application.

1. [Multi-stage builds](#1)
2. [What is Docker Compose?](#2)
3. [It's-a me, WordPress](#3)
4. [What are Docker-compose behaviors?](#4)
5. [What more can Docker-compose do?](#5)
6. [Use Docker-compose to run a dev env](#6)
7. [Docker-compose specification](#7)
8. [The factory container is back!](#8)

## Multi-stage builds <a name="1"></a>

![alt text](pictures/multi-stage.jpeg)

*Image: Quora.com*

Sometimes you need a lot of dependencies and libraries to build something inside a container, but then these are not necessary to actually run the target. This is true for compiled languages such C, C++, Rust, etc.

The consensus in the early age of Docker was to maintain two Dockerfile, one for development called *Dockerfile.build*, and one for running the built target, *Dockerfile*. It then required a shell script to copy the intermediate product into the new image (here with *build.sh* example). This is called the **builder pattern**.

### Dockerfile.build

This file is used to build the *app* binary:

```
# syntax=docker/dockerfile:1
FROM golang:1.16
WORKDIR /go/src/github.com/alexellis/href-counter/
COPY app.go ./
RUN go get -d -v golang.org/x/net/html \
  && CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .
```

### Dockerfile

This file is used to embed the *app* binary built previously into a new image: 

```
# syntax=docker/dockerfile:1
FROM alpine:latest  
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY app ./
CMD ["./app"] 
```

### Build.sh

The whole operation is not very complex, but it still requires a few lines to be run every time a new software is built:

```
#!/bin/sh
echo Building alexellis2/href-counter:build

docker build --build-arg https_proxy=$https_proxy --build-arg http_proxy=$http_proxy \  
    -t alexellis2/href-counter:build . -f Dockerfile.build

docker container create --name extract alexellis2/href-counter:build  
docker container cp extract:/go/src/github.com/alexellis/href-counter/app ./app  
docker container rm -f extract

echo Building alexellis2/href-counter:latest

docker build --no-cache -t alexellis2/href-counter:latest .
rm ./app
```

This solution is forcing people to maintain two Dockerfile and some scripts in parallel, which sometimes can be quite tedious. That's why, starting in February 2017, a new feature was progressively added to Docker: **multi-stage building**.

### Multi-stage building example

A **stage** in a Dockerfile describes all the **instructions between calls to the FROM instruction**, which is always the beginning of a new stage. This allows to chain build recipes while retaining the context of early stages to be able to use them in later stages.

#### All in one file

From the official Docker documentation [example](https://docs.docker.com/develop/develop-images/multistage-build/), the previous solution can now be reduced to a single Dockerfile and no shell script:

```
# syntax=docker/dockerfile:1
FROM golang:1.16
WORKDIR /go/src/github.com/alexellis/href-counter/
RUN go get -d -v golang.org/x/net/html  
COPY app.go ./
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

FROM alpine:latest  
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=0 /go/src/github.com/alexellis/href-counter/app ./
CMD ["./app"]  
```

#### Named build stages

Build stages in the Dockerfile are labeled with integers (`--from=0`), starting at 0 on the FROM instruction. But you can give them an explicit name with the AS instruction, and then refer to them using this name (`--from=builder`):

```
# syntax=docker/dockerfile:1
FROM golang:1.16 AS builder
WORKDIR /go/src/github.com/alexellis/href-counter/
RUN go get -d -v golang.org/x/net/html  
COPY app.go    ./
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

FROM alpine:latest  
RUN apk --no-cache add ca-certificates
WORKDIR /root/
# This extracts the app file built in the previous 'builder' stage
COPY --from=builder /go/src/github.com/alexellis/href-counter/app ./
CMD ["./app"]  
```

#### Continue from a previously named stage or image

you can get any file from any labeled stage in the same Dockerfile:

```
# syntax=docker/dockerfile:1
FROM alpine:latest AS builder
RUN apk --no-cache add build-base

FROM builder AS build1
COPY source1.cpp source.cpp
RUN g++ -o /binary source.cpp

FROM builder AS build2
COPY source2.cpp source.cpp
RUN g++ -o /binary source.cpp
```

You can even use an external image, again with the COPY command:

```
COPY --from=nginx:latest /etc/nginx/nginx.conf /nginx.conf
```

Keep this in mind next time you ship a docker image to someone!


## What is Docker Compose? <a name="2"></a>
[Docker Compose](https://docs.docker.com/compose/reference/) is an automation tool to run multi-service/container based applications. By loading a configuration file in [YAML](https://en.wikipedia.org/wiki/YAML) format, it allows for cross-platform management of several containers at once, and on-the-fly update of existing containers configuration. Using Docker Compose, it also becomes possible to define relationships between containers: shared networks, volumes, or startup order.

`I bet I could do this with a bunch of shell scripts!`

Maybe. On a few Linux systems. But Docker has always been about getting some abstraction from the platform OS, and with this goal Docker Compose has been made public almost as early as Docker itself, with the first public beta 0.0.1 released in December 2013, and the 1.0 production-ready version in October 2014. 

`I know a guy at Microsoft who does this with Kubernetes!`

![alt text](pictures/dilbert_kubernetes.jpeg)

*Image: Dilbert.com*

We will talk about Kubernetes next week. For now, keep in mind that Kubernetes is a **distributed container orchestration tool**. Docker Compose is located at a **lower level of abstraction**, to define interactions between containers in a declarative way instead of scripting calls to the Docker API. What **Docker Compose** YAML files define is semantically very close to a **Kubernetes Cluster**: something to run anywhere, on the cloud infrastructure. But **a Compose configuration is bound to be used on a single host**.

![alt text](pictures/compose_kubernetes.png)

*Image: theserverside.com*

Think of it like this: if Docker Compose was used for a vehicle definition 🚘, Kubernetes would then be used to organize a car racing contest 🛣.

If one day you want to deploy a cluster of containers (aka *pods* in Kubernetes), you first need to know how to define such a cluster!

## It's-a me, WordPress <a name="3"></a>

Remember [this](https://gitlab.com/docker_iut_2022/week_3/-/blob/main/README.md#run-your-own-wordpress) private Wordpress example? What does it take to do the same thing, using Docker Compose and a YAML configuration?

Not much:

```
version: '3.0'

services:
   db:
     image: mysql:5.7
     volumes:
       - db_data:/var/lib/mysql
     restart: always
     environment:
       MYSQL_ROOT_PASSWORD: somewordpress
       MYSQL_DATABASE: wordpress
       MYSQL_USER: wordpress
       MYSQL_PASSWORD: wordpress
     networks:
       - my-network

   wordpress:
     depends_on:
       - db
     image: wordpress:latest
     ports:
       - "8000:80"
     restart: always
     environment:
       WORDPRESS_DB_HOST: db:3306
       WORDPRESS_DB_USER: wordpress
       WORDPRESS_DB_PASSWORD: wordpress
     networks:
       - my-network

volumes:
    db_data:

networks:
    # This line is enough for a default network!
    # my-network: {}
    
    my-network:
        driver: bridge
        ipam:
            driver: default
            config:
            - subnet:  10.103.0.1/16
```

Try putting the above YAML section in a file named *docker-compose.yml* and load it with **docker-compose up** (the file is already in the *wordpress_v2/* folder):

```
cd wordpress_v2
docker-compose up -d
```

Docker-compose will pull the images and creates the containers, with the proper configuration. **From the folder containing the YAML file**:

```
raph@raph-VirtualBox:~/iut/week_4/wordpress_v2$ docker-compose up -d
Creating network "wordpress_v2_default" with the default driver
Creating volume "wordpress_v2_db_data" with default driver
Pulling db (mysql:5.7)...
5.7: Pulling from library/mysql
15115158dd02: Pull complete
d733f6778b18: Pull complete
1cc7a6c74a04: Pull complete
c4364028a805: Pull complete
82887163f0f6: Pull complete
097bfae26e7a: Pull complete
e1b044d6a24f: Pull complete
a924e739fa39: Pull complete
6bd08908162d: Pull complete
8a3523a22b20: Pull complete
26bb314e3485: Pull complete
Digest: sha256:5c6f1132190256d1ee63afc3bb383c890e8cb9f547bb1f8f15fecaa2a78e7de0
Status: Downloaded newer image for mysql:5.7
Pulling wordpress (wordpress:latest)...
latest: Pulling from library/wordpress
f7a1c6dad281: Pull complete
418d05f34fc8: Pull complete
12340edc305c: Pull complete
505a3ac77996: Pull complete
508288175cbf: Pull complete
55c636ebd5df: Pull complete
22c6b8d33038: Pull complete
cccff9c73797: Pull complete
afeecbf566d0: Pull complete
b548443661a4: Pull complete
72f7ec62b71e: Pull complete
4808d5297e6b: Pull complete
c2c7571acfb2: Pull complete
e8ecc30daa09: Pull complete
3c1b193d2787: Pull complete
823e3e1414fb: Pull complete
19dd60bfa8ef: Pull complete
d5c6a8e69185: Pull complete
5b66af88c668: Pull complete
eb1a09902add: Pull complete
61ba9d4485cc: Pull complete
Digest: sha256:bfef766f0372017edf32b8a8207e9fd420490a4973867d7b82bca9dc3180d97e
Status: Downloaded newer image for wordpress:latest
Creating wordpress_v2_db_1 ... done
Creating wordpress_v2_wordpress_1 ... done
```

The same Wordpress application as last week is now available once again.
**Docker-compose** works with an **interface very similar to docker**:

To **list the images currently used** in a Docker-compose configuration:

```
raph@raph-VirtualBox:~/iut/week_4/wordpress_v2$ docker-compose images
       Container           Repository    Tag       Image Id       Size  
------------------------------------------------------------------------
wordpress_v2_db_1          mysql        5.7      8b94b71dcc1e   448.2 MB
wordpress_v2_wordpress_1   wordpress    latest   b352e074cf4f   605.3 MB
```

This commande above also lists the containers created from these images, since a Docker-compose call means that even for a brief amount, you have instantiated containers from these images! This is a key difference with `docker images` command.

To list the **containers status** in this Docker-compose configuration:

```
raph@raph-VirtualBox:~/iut/week_4/wordpress_v2$ docker-compose ps
          Name                        Command               State                  Ports                
--------------------------------------------------------------------------------------------------------
wordpress_v2_db_1          docker-entrypoint.sh mysqld      Up      3306/tcp, 33060/tcp                 
wordpress_v2_wordpress_1   docker-entrypoint.sh apach ...   Up      0.0.0.0:8000->80/tcp,:::8000->80/tcp
```

If you look at the networks list, you can see the one Docker-compose created:

```
raph@raph-VirtualBox:~/iut/week_4/wordpress_v2$ docker network  ls
NETWORK ID     NAME                      DRIVER    SCOPE
505f0535453b   bridge                    bridge    local
83da3a79c000   host                      host      local
0bf2bb318b4c   none                      null      local
c7cb5981ddae   wordpress_v2_my-network   bridge    local
```

Finally, to **stop the application**, use the following command:

```
raph@raph-VirtualBox:~/iut/week_4/wordpress_v2$ docker-compose stop
Stopping wordpress_v2_wordpress_1 ... done
Stopping wordpress_v2_db_1        ... done
```

This will exit all the containers without destroying them, and remove the previously created networks. You can also use the **down** subcommand, and completely remove the container after bringing them down. By adding the **--volumes** option, you destroy all the volumes attached to this configuration:

```
docker-compose down --volumes
```


## What are Docker-compose behaviors? <a name="4"></a>

To follow up on volumes management: **Compose preserve all volumes** used by the services. When running the **up** subcommand, if it finds an existing volume from an old container, it will reuse it. **Data in volumes is not lost by default**.

Moreover, Compose also **preserves containers** and only recreates them if they have changed. This also to quickly reload Compose configurations.

Compose supports variable [substitution](https://docs.docker.com/compose/compose-file/compose-file-v3/#variable-substitution) in its YAML file, and you can customize the YAML file based on your environment state.


## What more can Docker-compose do? <a name="5"></a>

Most of the time, you will only need 4 subcommands of Docker-compose: **up**, **down**, and sometimes **ps** and **build**.

You may have noticed the **version field. It is not a trivial information**: the Docker-compose specification changed over time, and the later the version, the better your options in defining your containerized application. The Docker Engine **>=19.03 is compatible with the compose >=3.8** file format versions. The file format changelog is available [here](https://docs.docker.com/compose/compose-file/compose-versioning/#version-3).

What is the **build** subcommand about? It is useful if you build locally the images you are about to use. An example is in the *myApp/* folder:

```
version: '3.0'

services:
  web:
    build: .
    ports:
     - "5000:5000"
  redis:
    image: "redis:alpine"
```

You can see the **build** keyword with the current folder relative path. 
It means the Docker image has to be built using the given build context. 
You can trigger a build of the images used in the services, without running them, using the **build** subcommand:

```
cd myApp
docker-compose build
```

This example would produce the following output:

```
raph@raph-VirtualBox:~/iut/week_4/myApp$ docker-compose build
redis uses an image, skipping
Building web
Step 1/5 : FROM python:3.9.10-alpine3.15
 ---> f62772d5daae
Step 2/5 : ADD . /code
 ---> 94ac420698dc
Step 3/5 : WORKDIR /code
 ---> Running in 9614d96112ac
Removing intermediate container 9614d96112ac
 ---> 7f5fc76b0b9b
Step 4/5 : RUN pip install -r requirements.txt
 ---> Running in 06123469f324
Collecting flask
  Downloading Flask-2.0.3-py3-none-any.whl (95 kB)
Collecting redis
  Downloading redis-4.1.4-py3-none-any.whl (175 kB)
Collecting itsdangerous>=2.0
  Downloading itsdangerous-2.1.0-py3-none-any.whl (15 kB)
Collecting click>=7.1.2
  Downloading click-8.0.4-py3-none-any.whl (97 kB)
Collecting Werkzeug>=2.0
  Downloading Werkzeug-2.0.3-py3-none-any.whl (289 kB)
Collecting Jinja2>=3.0
  Downloading Jinja2-3.0.3-py3-none-any.whl (133 kB)
Collecting deprecated>=1.2.3
  Downloading Deprecated-1.2.13-py2.py3-none-any.whl (9.6 kB)
Collecting packaging>=20.4
  Downloading packaging-21.3-py3-none-any.whl (40 kB)
Collecting wrapt<2,>=1.10
  Downloading wrapt-1.13.3-cp39-cp39-musllinux_1_1_x86_64.whl (80 kB)
Collecting MarkupSafe>=2.0
  Downloading MarkupSafe-2.1.0-cp39-cp39-musllinux_1_1_x86_64.whl (29 kB)
Collecting pyparsing!=3.0.5,>=2.0.2
  Downloading pyparsing-3.0.7-py3-none-any.whl (98 kB)
Installing collected packages: wrapt, pyparsing, MarkupSafe, Werkzeug, packaging, Jinja2, itsdangerous, deprecated, click, redis, flask
Successfully installed Jinja2-3.0.3 MarkupSafe-2.1.0 Werkzeug-2.0.3 click-8.0.4 deprecated-1.2.13 flask-2.0.3 itsdangerous-2.1.0 packaging-21.3 pyparsing-3.0.7 redis-4.1.4 wrapt-1.13.3
WARNING: Running pip as the 'root' user can result in broken permissions and conflicting behaviour with the system package manager. It is recommended to use a virtual environment instead: https://pip.pypa.io/warnings/venv
WARNING: You are using pip version 21.2.4; however, version 22.0.4 is available.
You should consider upgrading via the '/usr/local/bin/python -m pip install --upgrade pip' command.
Removing intermediate container 06123469f324
 ---> 272b516bef54
Step 5/5 : CMD ["python", "app.py"]
 ---> Running in 0829aa7894bc
Removing intermediate container 0829aa7894bc
 ---> 7b459fe635e7
Successfully built 7b459fe635e7
Successfully tagged myapp_web:latest
```

What if we want to give a **specific Dockerfile** for an app?

```
app1:
  build: .
  dockerfile: Dockerfile_app1
  ports:
   - "80:80"
app2:
  build: .
  dockerfile: Dockerfile_app2
  ports:
   - "80:80"
```

The build context can be defined in an **even more explicit** manner:

```
version: "3.9"
services:
  webapp:
    build: ./dir
```

is equivalent to:

```
version: "3.9"
services:
  webapp:
    build:
      context: ./dir
	  dockerfile: Dockerfile
```

By default the **service name is also the built image name**, but it can be **overriden** with the image keyword:

```
build: ./dir
image: webapp:tag
```

You can also give **extra args** to the build step, the same way you would with the docker client using *--args VAR=VAL* option:

```
services:
  webapp:
    build:
      context: ./dir
	  # mapping format
      args:
        buildno: 1
		bar: foobar
```

This syntax is also available:

```
services:
  webapp:
    build:
      context: ./dir
      # list format
      args:
	    - buildno: 1
	    -bar: foobar
```

Now let's run the *myApp/* folder example, **in attached mode**:

```
cd myApp
docker-compose up
```

One of the output line should look like this:

```
web_1    |  * Running on http://172.24.0.2:5000/ (Press CTRL+C to quit)
```

Follow this link, and you will see a small webpage is now hosted by this application:

![alt text](pictures/webapp.png)
Each refresh will increment the application counter.

### Use Docker-compose to run a dev env <a name="6"></a>

This little webpage refreshing itself is a simple example of a working environment deployed with a one-liner using Docker-Compose.

It is even more interesting to make the application sources available from outside the dockerized environment, this way a developper can launch IDEs and other tools, edit the code, and then compile/apply the changes in real-time without having to reload containers.

Add the following section to the web service in myApp/docker-compose.yml:

```
    volumes:
      - .:/code
    environment:
      FLASK_ENV: development
```

Then relaunch the application using Compose:

```
cd myApp
docker-compose restart
```

If the configuration file is updated, restart will reload services with the latest configuration.

Did you notice you can use relative path for volumes in Docker-Compose YAML files? The reason is because you declare volumes below services, so unlike with the docker client, Compose can separate the two types of mount.

Adding the current folder to the service as a mounted path is a very common setting for development, this way the dev tools can be in the host environment, and the compilation tools in the container(s).

Example: now that you restarted the containers from *myApp/*, try to change the *app.py* in it, and refresh the webpage to see your changes in real-time:


![alt text](pictures/app_before_changes.png)

```
cd myApp/
sed "s/Hello world/Hello new world/" app.py -i
```

![alt text](pictures/app_after_changes.png)



## Docker-compose specification <a name="7"></a>

The keywords used earlier are a tiny part of the whole Docker-compose specification, which is available on the [compose-spec](https://github.com/compose-spec/compose-spec/blob/master/spec.md) Github repository. Feel free to also look at the [vision](https://github.com/compose-spec/compose-spec/blob/master/VISION.md) document in the same repository to learn more about Docker-compose purpose.

## The factory container is back! <a name="8"></a>

Using the previous examples, try again to **link the containers from the factory image seen in the previous week using Docker-Compose**. The maximum reachable score stored in the containers volume *warehouse* file is 625.
The *factory/* folder is available in this repo too. 

## Author
Raphaël Bouterige
