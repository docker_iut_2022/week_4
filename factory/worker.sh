readonly INPUT_IP=$(/sbin/ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}')
readonly VOLUME_ERROR=1
readonly CONCURRENCY_ERROR=2
readonly MAX_WORKERS_N=5
readonly CONTAINER_ID=$(tail -n 1 /etc/hostname)
RANDOM_PORT=$(( ( RANDOM % 10 )  + 2000 ))
readonly INPUT_PORT="${INPUT_PORT:-$RANDOM_PORT}"
RANDOM_PORT=$(( RANDOM_PORT  + 1 ))
readonly OUTPUT_PORT="${OUTPUT_PORT:-$RANDOM_PORT}"
readonly OUTPUT_IP="${OUTPUT_IP:-}"
readonly WAIT_S="${WAIT_S:-10}"


function log()
{
    echo "[ $(date +%s) ] $@"
}

function print_params()
{
    log "Creating new worker $CONTAINER_ID for the factory"
    log "INPUT_IP = $INPUT_IP"
    log "OUTPUT_IP = $OUTPUT_IP"
    log "INPUT_PORT = $INPUT_PORT"
    log "OUTPUT_PORT = $OUTPUT_PORT"
    log "WAIT_S = $WAIT_S"
}

function change_for_work()
{
    if [ ! -d /factory ] ; then
        log "The worker can't prepare: /factory does not exist!"
        exit $VOLUME_ERROR
    fi

    # Create factory locker room if it does not exist yet in factory volume
    mkdir -p /factory/locker_room

    log "The worker enters the /factory/locker_room ..."

    if [ $(ls /factory/locker_room | wc -l) -ge $MAX_WORKERS_N ] ; then
        log "The worker can't prepare: /factory/locker_room is full!"
        exit $CONCURRENCY_ERROR
    else
        locker_key="/factory/locker_room/worker.$CONTAINER_ID"
        touch "$locker_key"
        log "The worker gets a locker with a worksuit. "
        log "Locker key is $locker_key"
        log "Currently $(ls /factory/locker_room | wc -l) worker(s) in factory"
    fi
}

function wait_for_raw_materials()
{
    log "Waiting for raw materials delivery on $INPUT_IP:$INPUT_PORT..."
    python3 /scripts/server.py $INPUT_IP $INPUT_PORT $WAIT_S

    raw_units=$(cat /tmp/goods 2> /dev/null)

    if [ -z "$raw_units" ] ; then
        log "No raw materials delivery during this shift"
        log "The worker only managed to produce one unit of goods"
        local goods=1
    else
        log "Raw materials were delivered during this shift!"
        log "The worker produced five units of goods per received unit"
        local goods=$((5 * raw_units))
        log "Received $raw_units units, produced $goods from it"
    fi

    if [ -z "$OUTPUT_IP" ] ; then
        log "No destination factory set for produced goods"
        log "Storing $goods good(s) in /factory/warehouse"
        log "$goods" >> /factory/warehouse
    else
        log "Destination factory for these goods: $OUTPUT_IP:$OUTPUT_PORT"
        send_goods $goods
    fi
}

function send_goods()
{
    local goods=$1
    # Send what will become another factory delivery
    python3 /scripts/client.py $OUTPUT_IP $OUTPUT_PORT $goods
}

print_params

log "Worker is going to the factory"

change_for_work

wait_for_raw_materials

log "Work is finished!"

log "The worker puts back the worksuit and goes home. "

rm "$locker_key"

